# Compiler and flags
CC = g++
CC_FLAGS = -g -Wall -fexceptions -O2 -pedantic

# The build target executable
TARGET = as_hex2syx

# The directory where the target will be built
BUILD_DIR = build

all: dir
	cd $(BUILD_DIR) && $(CC) $(CC_FLAGS) -o $(TARGET) ../main.cpp 

dir:
	mkdir -p $(BUILD_DIR)

clean:
	rm -r $(BUILD_DIR)
