#include <iostream>
#include <fstream>
#include "hex.h"

using std::cout;
using std::endl;
using std::ofstream;
using std::ios;

// hard-coding this to work specifically with atmega2561 because we don't care about other cases
#define PAGE_SIZE (128*2) // 128 "words"
#define NUM_PAGES 1024
#define TOTAL_MEM (PAGE_SIZE*NUM_PAGES)
// the max bootloader size on the atmega2561 is 8kB which is 32 pages
// so let's define the max firmware size: (1024-32) pages
#define FW_MAX_SZ (PAGE_SIZE*(NUM_PAGES-32))


int main(int argc, char *argv[])
{
    cout << "\n" << "..:: hex2syx v1.00 ::..\n";

    if (argc <= 1)
    {
        // print usage

        cout << "Usage: " << argv[0] << " [-r] <firmware.hex>\n";
        cout << "  A file with appended .syx extension will be generated.\n";
        cout << "  If the -r parameter is specified, a raw binary file (with .raw appended to the name) will be generated instead.\n";
        cout << "  If the file exist, it will be overwritten without confirmation.\n";
        cout << "This tool is specifically made to work with the x0xb00t2.1 bootloader. ";
        cout << "It is NOT an universal hex to sysex converter.\n";
        cout << "Made by Anton Savov - (antto@mail.bg).\n";

        return 0;
    }

    bool raw = false;
    string fn;
    //if (argc > 1)
    {
        string prm;
        uint16_t i = 1;
        while (i < argc)
        {
            prm = argv[i];
            if ((i+1) == argc) // if this is the last param, it MUST be the filename
            {
                fn = prm;
            }
            else if (prm.compare("-r") == 0) { raw = true; }
            else { cout << "\n* Unknown argument: \"" << prm << '\"'; }
            ++i;
        }
    }

    HEX hex(TOTAL_MEM);
    cout << "*** Processing \"" << fn << "\" ... ";
    uint8_t res = hex.load(fn.c_str());
    if (res == 1)
    {
        if (hex.szsz > 0)
        {
            cout << "okay.\n";
            cout << "*** " << hex.szsz << " bytes of data.\n";
            if (hex.szsz <= FW_MAX_SZ)
            {
                cout << "*** Start: " << hex.get_start() << "\n";
                if (hex.get_start() == 0)
                {
                    res = 2; // okay
                }
                else { cout << "Error: expected start from zero. Is this a bootloader hex?\n"; }
            }
            else { cout << "Error: data size is too big. Expected <= " << FW_MAX_SZ << " bytes.\n"; }
        }
        else { cout << "Error: the hex file is empty.\n"; }
    }
    else { cout << "Error: couldn't read the hex file.\n"; }

    if (res == 2)
    {
        // hex has been loaded and is "okay"
        // it's time to export it to .syx
        // first, generate the filename for it
        // the file will be overwritten!
        string fn2 = fn;
        if (raw)    { fn2 += ".raw"; }
        else        { fn2 += ".syx"; }
        cout << "\n";
        cout << "*** Output file: \"" << fn2 << "\". The file will be overwritten.\n";

        // open in binary mode
        ofstream f(fn2.c_str(), ios::binary);
        if (f.is_open())
        {
            cout << "Writing... ";

            if (raw)
            {
                // write the binary data at once, heh ;]
                f.write(reinterpret_cast<const char*>(hex.buf),hex.szsz);
            }
            else
            {
                // Sysex: F0 [ID ID ID] [DATA] F7
                // i'll use 7D 03 03 for the ID as SokkOS 2.0
                // then 7F 7F 7F for firmware update
                // F0 7D 03 03 7F 7F 7F <data> F7
                static const char sysex_start   = 0xF0;
                static const char sysex_end     = 0xF7;

                static const uint8_t head[6] = { 0x7D, 0x03, 0x03, 0x7F, 0x7F, 0x7F };

                // the very first sysex message will NOT contain any binary data
                // it will be used to tell the bootloader to prepare for programming

                f.write(&sysex_start,1);
                f.write(reinterpret_cast<const char*>(head),6);
                f.write(&sysex_end,1);

                // now the actual raw data

                uint32_t page = 0;
                uint32_t offs = 0;
                uint32_t i = 0;
                uint8_t buf[PAGE_SIZE*2];
                while (offs < hex.szsz)
                {
                    i = 0;
                    while (i < PAGE_SIZE)
                    {
                        uint8_t x = hex.buf[offs+i];
                        buf[  i+i] = x&0x0F;
                        buf[1+i+i] = (x>>4);
                        ++i;
                    }

                    f.write(&sysex_start,1);
                    f.write(reinterpret_cast<const char*>(head),6);
                    f.write(reinterpret_cast<const char*>(buf),PAGE_SIZE*2);
                    f.write(&sysex_end,1);
                    offs += PAGE_SIZE;
                }

                // well, let's add another empty message at the very end
                // to tell the bootloader we're finished

                f.write(&sysex_start,1);
                f.write(reinterpret_cast<const char*>(head),6);
                f.write(&sysex_end,1);
            }
            if (!f.good())
            {
                cout << "\nError: some error occured while writing the output file.\n";
            }
            else
            {
                cout << "Done.\n";
            }
            f.close();
        }
        else { cout << "Error: could not open the output file.\n"; }
    }
    cout << "----\n";
    return 0;
}
