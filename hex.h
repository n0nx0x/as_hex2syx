#ifndef HEX_H_INCLUDED
#define HEX_H_INCLUDED

// this code taken from c0nb0x

#include <fstream>
#include <string>
using std::string;
using std::ifstream;

struct dbuf
{
    dbuf(const uint16_t size)
    {
        sz = 0;
        buf = 0;
        buf = new uint8_t[size];
        if (buf != 0) { sz = size; }
    }
    inline void fill(const uint8_t v = 0xFF)
    {
        uint16_t i = sz;
        while (i--) { buf[i] = v; }
    }
    ~dbuf()
    {
        if (buf != 0) { delete [] buf; buf = 0; sz = 0; }
    }
    uint8_t* buf;
    uint16_t sz;
};

class HEX
{
    public:
    HEX(const uint32_t size)
    {
        sz = 0;
        buf = 0;
        buf = new uint8_t[size];
        if (buf != 0)
        {
            sz = size;
        }
        uint32_t i = sz;
        while (i--) { buf[i] = 0xFF; }
    }
    ~HEX()
    {
        if (buf != 0) { delete [] buf; buf = 0; }
    }

    bool load(const char* fn)
    {

        bool res = false;
        string line;
        string tok;

        uint8_t hlen;
        uint16_t hoff;
        uint8_t htyp;
        uint8_t chks;

        uint32_t addr = 0;
        uint32_t di = 0;
        uint16_t ri = 0;

        start = sz;
        end = 0;

        szsz = 0;
        //uint32_t ddd = 0; // DEBUG
        errc = 0x00;

        ifstream f(fn);
        if (f.is_open())
        {
            //res = true;
            while (1)
            {
                f >> line;
                {
                    if (line.length() < 11) { break; }
                    if (line[0] != ':') { break; }
                    tok.clear(); tok = line.substr(1, 2);
                    if (unHEXify(tok, hlen) == false) { break; }
                    tok.clear(); tok = line.substr(3, 4);
                    if (unHEXify(tok, hoff) == false) { break; }
                    tok.clear(); tok = line.substr(7, 2);
                    if (unHEXify(tok, htyp) == false) { break; }

                    if (line.length() < (hlen+hlen+11)) { break; }

                    chks = hlen;
                    chks += ((hoff >> 8) & 0xFF);
                    chks += ((hoff     ) & 0xFF);
                    chks += htyp;
                }
                // #####################
                {
                    dbuf db(hlen);
                    szsz += hlen;
                    ri = 0;
                    while (ri < hlen)
                    {
                        uint8_t tmp;
                        tok.clear(); tok = line.substr((ri+ri+9), 2);
                        if (unHEXify(tok, tmp) == false) { break; }
                        db.buf[ri] = tmp;
                        chks += tmp;
                        ++ri;
                    }

                    {
                        uint8_t tmp;
                        tok.clear(); tok = line.substr((ri+ri+9), 2);
                        if (unHEXify(tok, tmp) == false) { break; }
                        chks += tmp;
                        if (chks != 0) { break; }
                    }
                    // done with parsing..

                    if (htyp == 0x00)
                    {
                        // data
                        if ((addr + hoff + hlen) > sz)
                        {
                            errc = 0x01; // more data than our buffer size, abort
                            break;
                        }
                        di = 0;
                        while (di < hlen)
                        {
                            buf[addr + hoff + di] = db.buf[di];
                            ++di;
                            //++ddd; // DEBUG
                            // so if start=0 - "end" is NOT the size, it's actually size-1 for some weird reason
                        }
                        if ((addr + hoff) < start) { start = addr + hoff; }
                        if ((addr + hoff + hlen - 1) > end) { end = addr + hoff + hlen - 1; }
                    }
                    else if (htyp == 0x01)
                    {
                        // eof
                        res = true;
                        break;
                    }
                    else if (htyp == 0x02)
                    {
                        addr = (db.buf[0] << 8) | db.buf[1];
                        addr = (addr << 4);
                    }
                    else if (htyp == 0x04)
                    {
                        addr = (db.buf[0] << 8) | db.buf[1];
                        addr = (addr << 16);
                    }
                    else { break; }
                }
                // ######################

                if (f.eof() != false) { break; }
            }
            f.close();
        }

        return res;
    }
    inline const uint32_t get_start() const { return start; }
    inline const uint32_t get_end() const { return end; }
    inline const uint32_t get_end2() const { return end+1; } // with start=0 - this would be the end

    uint8_t* buf; // mehehe, public buffer ;]
    uint32_t szsz;
    uint8_t errc;
    // ##########################
    private:
    bool unHEXify(const string &s, uint16_t &v)
    {
        uint32_t x;
        bool res = unHEXify(s, x);
        v = x & 0x0000FFFF;
        return res;
    }
    bool unHEXify(const string &s, uint8_t &v)
    {
        uint32_t x;
        bool res = unHEXify(s, x);
        v = x & 0x000000FF;
        return res;
    }
    bool unHEXify(const string &s, uint32_t &v)
    {
        v = 0x00000000;
        uint8_t i = 0;
        uint8_t nib;
        if (s.length() >= (sizeof(v) * 2)) { return false; }
        while (i < s.length())
        {
            nib = s[i];
            if      ((nib >= '0') && (nib <= '9')) { nib -= '0'; }
            else if ((nib >= 'A') && (nib <= 'F')) { nib += (10 - 'A'); }
            else if ((nib >= 'a') && (nib <= 'f')) { nib += (10 - 'a'); }
            else { return false; }
            v = (v << 4);
            v = v | nib;
            ++i;
        }
        return true;
    }
    // -----------
    uint32_t start, end;
    uint32_t sz;
};


#endif // HEX_H_INCLUDED
